import ExperienceItem, { ExperienceItemProps } from 'apps/resume/components/experience-item/experience-item';
import { DateTime } from 'luxon';

/* eslint-disable-next-line */
export interface EducationSectionProps {}

export function EducationSection(props: EducationSectionProps) {
  const experienceItems: ExperienceItemProps[] = [
    {
      title: 'Bachelor of Information Technology (Hons)',
      company: 'Ontario Tech University',
      companyNameCss: 'hover:text-ontariotech',
      companyLink: 'https://ontariotechu.ca/',
      descriptions: [
        'Majored in Networking and IT Security with Minor in Operation Management',
        "Received President's List and Dean's List award",
        'Created blockchain in Python with Secure Contract Execution (see Peth in Project Section)',
        'Deployed and troubleshot multiple OpenStack environments, Networks (OSPF, EIGRP, BGP, Spanning Tree) and Active Directory',
        'Created Security Audit Reports from Web Penetration tests',
      ],
      from: DateTime.fromISO('2016-09-01'),
      to: DateTime.fromISO('2020-04-30'),
    },
   /* {
      title: 'Ontario Secondary School Diploma',
      company: 'R. H. King Academy',
      companyNameCss: 'hover:text-rhking',
      companyLink: 'https://schoolweb.tdsb.on.ca/rhkingacademy',
      descriptions: [
        'Received Robotics and Control Systems award and Ontario Scholar award',
        'Secured $10,000 in funding for Robotics',
        'Demonstrated SQL injection and Brute Force vulnerabilities in school website',
      ],
      from: DateTime.fromISO('2019-07-01'),
      to: DateTime.fromISO('2019-12-31'),
    },
    {
      title: '',
      company: 'Marc Garneau Collegiate Institute',
      companyNameCss: 'hover:text-marcgarneau',
      companyLink: 'http://schools.tdsb.on.ca/marcgarneau/',
      descriptions: [
        'Developed traffic light system in C++ utilizing parallel ports',
      ],
      from: DateTime.fromISO('2019-07-01'),
      to: DateTime.fromISO('2019-12-31'),
    },*/
  ];
  return (
    <div className={`text-slate-900`}>
      <h2 className="text-2xl pl-2">Education</h2>

      <hr className="mt-1 mb-3"></hr>

      <div className="px-2 grid gap-3 ">
        {experienceItems.map((item, key) => (
          <ExperienceItem {...item} key={key}/>
        ))}
      </div>
    </div>
  );
}

export default EducationSection;
