import ExperienceItem, { ExperienceItemProps } from 'apps/resume/components/experience-item/experience-item';
import { DateTime } from 'luxon';

/* eslint-disable-next-line */
export interface ProjectsSectionProps {}

export function ProjectsSection(props: ProjectsSectionProps) {
  const experienceItems: ExperienceItemProps[] = [
    {
      title: 'Unwrapper',
      company: '(https://youtu.be/UaPaD6krKmU)',
      companyNameCss: 'hover:text-black',
      companyLink: 'https://youtu.be/UaPaD6krKmU',
      descriptions: [
        'Created a Ethereum/Polygon bridge fee aggregator to reduce individual bridge fees by 50% using Go Ethereum, Express, Angular and PostgreSQL',
      ],
      from: DateTime.fromISO('2020-11-01'),
      to: DateTime.fromISO('2020-12-31'),
    },
    {
      title: 'Peth',
      company: '(https://gitlab.com/AzhanL/peth)',
      companyNameCss: 'hover:text-black',
      companyLink: 'https://gitlab.com/AzhanL/peth',
      descriptions: [
        'Developed blockchain in Python that facilitates secure contract execution using Python Classes',
        'Decompiled and analyzed Python op-codes to implement security'
      ],
      from: DateTime.fromISO('2019-11-01'),
      to: DateTime.fromISO('2019-11-30'),
    },
  ];
  return (
    <div className={`text-slate-900`}>
      <h2 className="text-2xl pl-2">Past Projects</h2>

      <hr className="mt-1 mb-3"></hr>

      <div className="px-2 grid gap-3 grid-cols-1 md:grid-cols-2 print:grid-cols-2">
        {experienceItems.map((item, key) => (
          <ExperienceItem {...item} key={key}/>
        ))}
      </div>
    </div>
  );
}

export default ProjectsSection;
