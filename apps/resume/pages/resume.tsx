import Header from '../components/header/header';
import CertificationsSection from './certifications-section';
import EducationSection from './education-section';
import ExperienceSection from './experience-section';
import ProjectsSection from './projects-section';
import SkillsSection from './skills-section';

export function Resume() {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./index.scss file.
   */
  return (
    <div style={{ margin: 'auto', maxWidth: '777px' }} className="mx-auto py-5 print:py-0">
      <Header />
      <div className='bg-violet-600 rounded-full my-3' style={{ height: '3.82px'}}></div>
      
      <ExperienceSection />
      <div className='my-5'></div>
      <SkillsSection />
      <div className='my-5'></div>
      <EducationSection />
      <div className='my-5'></div>
      <ProjectsSection />
      <div className='my-5'></div>
      <CertificationsSection />
    </div>
  );
}

export default Resume;
