import { DateTime, Interval } from "luxon";
import { useMemo } from "react";

export interface ExperienceItemProps {
  title: string;
  company: string;
  companyLink: string;
  companyNameCss:
    | 'hover:text-bestathletes'
    | 'hover:text-rkpublishing'
    | 'hover:text-attorneygeneral'
    | 'hover:text-ontariotech'
    | 'hover:text-rhking'
    | 'hover:text-marcgarneau'
    | string
    ;
  from: DateTime;
  to: DateTime | null;
  descriptions: string[];
}

export function ExperienceItem(props: ExperienceItemProps) {
  const isEmptyHeader = !(props.title !== '' && props.from !== null)
  const duration = useMemo(() => {
    const from = props.from
    const to = props.to === null ? DateTime.now() : props.to
    const intervalDuration = Interval.fromDateTimes(from, to).toDuration(['years', 'months']);
    const output = []
    if (intervalDuration.years !== 0)
      output.push(`${intervalDuration.years} ${intervalDuration.years == 1 ? 'year' : 'years'}`) 
    if (intervalDuration.months !== 0)
      output.push(`${Math.round(intervalDuration.months)} ${intervalDuration.months == 1 ? 'month' : 'months'}`) 
    return output.join(', ')
  }, [props.from, props.to])
  return (
    <div className={isEmptyHeader ? '-mt-3' : ''}>
      <div className="grid grid-cols-3 auto-rows-max ">
        {
          !isEmptyHeader &&
          <>
            <h3 className="text-xl font-semibold col-span-2" >{props.title}</h3>
            <p className="text-right self-center">
              <i className="far fa-calendar-alt mr-2"></i>
              {props.from.toFormat('LLL, yyyy')} 
              {(props.to !== null) ? <> − {props.to.toFormat('LLL, yyyy')}</> : ' − Present'}
            </p>
          </>
        }
        <h4 className="text-lg col-span-2">
          <a
            href={props.companyLink}
            className={`${props.companyNameCss} transition-colors duration-300 hover:cursor-pointer`}
          >
            {props.company}
          </a>
        </h4>
        <p className="text-right self-center text-sm text-slate-600">
          ({duration})
        </p>
      </div>
      <ul className="list-disc ml-5">
        {props.descriptions.map((description, index) => (
          <li key={index}>{description}</li>
        ))}
      </ul>
    </div>
  );
}

export default ExperienceItem;
