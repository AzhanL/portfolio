import Header from '../components/header/header';
import EducationSection from './education-section';
import ExperienceSection from './experience-section';
import ProjectsSection from './projects-section';
import SkillsSection from './skills-section';

export function Index() {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./index.scss file.
   */
  return (
    <div className='grid h-screen justify-center'>
      <div className='self-center text-center grid gap-3 grid-cols-2'>
        <div className='p-5 col-span-full'>
          <h1 className='text-5xl underline underline-offset-4'>Azhan Latif</h1>
        </div>
        <a href="/resume" className='group hover:cursor-pointer hover:bg-gray-200 duration-300 rounded-md'>
          <h4 className='p-3 text-xl'>
              Resume
            <i className="fas fa-file-alt ml-2 group-hover:text-violet-600 duration-300"></i>
          </h4>
        </a>
        <a href="https://gitlab.com/azhanl" className='group hover:cursor-pointer hover:bg-gray-200 duration-300 rounded-md'>
          <h4 className='p-3 text-xl'>
            Gitlab
            <i className="fa-brands fa-gitlab ml-2 group-hover:text-orange-600 duration-300"></i>
          </h4>
        </a>
        <footer className='col-span-full text-sm opacity-70 group mt-5'>
          Made with 
          <a href="https://reactjs.org/" className='hover:cursor-pointer'>
            <i className="fab fa-react mx-1 group-hover:text-react duration-300 font-semibold" title='React'></i> 
          </a>,
          <a href="https://tailwindcss.com/" className='hover:cursor-pointer'>
            <i className="mdi mdi-tailwind mx-1 group-hover:text-tailwind duration-300" title='Tailwind'></i>
          </a>
          and 
          <i className="fas fa-heart mx-1 group-hover:text-red-600 duration-300" title='Love'></i>
            from
          <a href="https://www.canada.ca/en.html" className='hover:cursor-pointer'>
            <i className="fab fa-canadian-maple-leaf mx-1 group-hover:text-canada duration-300" title='Canada'></i>
          </a>
        </footer>
      </div>
    </div>
  );
}

export default Index;
