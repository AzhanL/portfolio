import { render } from '@testing-library/react';

import ExperienceItem, { ExperienceItemProps } from './experience-item';
import { DateTime } from 'luxon';

describe('ExperienceItem', () => {
  it('should render successfully', () => {
    const dummyExperience: ExperienceItemProps = {
      title: "Dream Job",
      company: "Dream Company",
      companyLink: "https://dreamcompany.com",
      companyNameCss: "hover:text-bestathletes",
      from: DateTime.now(),
      to: null,
      descriptions: [],
    };
    const { baseElement } = render(<ExperienceItem  {...dummyExperience}/>);
    expect(baseElement).toBeTruthy();
  });
});
