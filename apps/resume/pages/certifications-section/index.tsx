import styles from './index.module.scss';

/* eslint-disable-next-line */
export interface CertificationsSectionProps {}

export function CertificationsSection(props: CertificationsSectionProps) {
  return (
    <div className={`text-slate-900`}>
    <h2 className="text-2xl pl-2">Certifications</h2>

    <hr className="mt-1 mb-1"></hr>

    <div className="px-2">
      <ul className="list-disc ml-5">
        <li>Microsoft Technology Associate: Networking Fundamentals (MTA)</li>
      </ul>
    </div>
  </div>
  );
}

export default CertificationsSection;
