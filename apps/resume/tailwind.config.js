const { join } = require('path');
const { createGlobPatternsForDependencies } = require('@nrwl/next/tailwind');
module.exports = {
  presets: [require('../../tailwind-workspace-preset.js')],
  content: [
    join(__dirname, './**/*.{js,ts,jsx,tsx}'),
    ...createGlobPatternsForDependencies(__dirname)
  ],
  theme: {
    extend: {
      colors: {
        'ramseytheory': '#5b606b',
        'bestathletes': '#84C6E4',
        'rkpublishing': '#EE1C25',
        'attorneygeneral': '#CABB93',
        'ontariotech': '#003C71',
        'rhking': '#284891',
        'marcgarneau': '#d4145a',
        'react': '#61DAFB',
        'canada': '#EF3340',
        'tailwind': '#38bdf8',
      }
    },
    fontFamily: {
      'sans': 'PT Sans'
    }
  },
  plugins: [],
  mode: 'jit',
}
