/* eslint-disable-next-line */
export interface SkillsSectionProps {}

export function SkillsSection(props: SkillsSectionProps) {
  return (
    <div className={`text-slate-900`}>
      <h2 className="text-2xl pl-2">Technical Skills</h2>

      <hr className="mt-1 mb-1"></hr>

      <div className="px-2">
        <ul className="list-disc ml-5">
          <li>
            Proficient in programming languages: Python, TypeScript, and Java
          </li>
          <li>Over 10 years of experience as a Linux user</li>
          <li>
            Skilled in SQL, NoSQL, and Elasticsearch queries; experienced with
            PostgreSQL, MySQL, and MongoDB
          </li>
          <li>
            Familiar with containerization and orchestration technologies:
            Docker, Kubernetes, KVMs, and OpenStack
          </li>
          <li>
            Competent in web frameworks and libraries: Django, Express, Vue.js,
            React, and React Native
          </li>
          <li>
            Knowledgeable in Domain Driven Design (DDD) and Test Driven
            Development (TDD) methodologies
          </li>
        </ul>
      </div>
    </div>
  );
}

export default SkillsSection;
