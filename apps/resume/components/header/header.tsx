import styles from './header.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGitlab, faLinkedin, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faMobileScreenButton, faPhone, faPhoneSquare } from '@fortawesome/free-solid-svg-icons';

/* eslint-disable-next-line */
export interface HeaderProps {}

export function Header(props: HeaderProps) {
  return (
    <div className="grid grid-cols-2 text-slate-800 transition-all">
        <h1 className='self-center text-6xl'>Azhan Latif</h1>

        <p className='text-right align-middle grid' style={{ lineHeight: '1.382rem' }}>
    
          <a href="mailto:info@azla.co" className='hover:text-indigo-800 duration-300 hover:cursor-pointer'>
            info@azla.co
            <i className="fa-regular fa-envelope" style={{ marginLeft: '6px' }}></i>         
          </a>
          <a href="tel:+19739289136" className='hover:text-green-500  duration-300 hover:cursor-pointer'>
            973-928-9136
            <i className="fa-solid fa-mobile-screen-button " style={{ marginRight: '2px', marginLeft: '8px'}}></i>
          </a>
          <a href="https://azhan.dev" className='hover:text-blue-500  duration-300 hover:cursor-pointer'>
            azhan.dev
            <i className="fas fa-globe ml-2"></i>
          </a>
          <a href="https://www.linkedin.com/in/azhanl" className='hover:text-sky-600 duration-300 hover:cursor-pointer'>
            linkedin.com/in/azhanl
            <i className="fa-brands fa-linkedin-in" style={{ marginLeft: '9px', marginRight: '1px'}}></i>
          </a>
          {/* <a href="https://gitlab.com/AzhanL" className='hover:text-orange-600 duration-300 hover:cursor-pointer'>
            gitlab.com/AzhanL
            <i className="fa-brands fa-gitlab ml-2"></i>
          </a> */}
        </p>

    </div>
  );
}

export default Header;
