import ExperienceItem, {
  ExperienceItemProps,
} from 'apps/resume/components/experience-item/experience-item';
import { DateTime } from 'luxon';
/* eslint-disable-next-line */
export interface ExperienceSectionProps {}

export function ExperienceSection(props: ExperienceSectionProps) {
  const experienceItems: ExperienceItemProps[] = [
    {
      title: 'Head of Engineering',
      company: 'Ramsey Theory Group',
      companyNameCss: 'hover:text-ramseytheory',
      companyLink: 'https://www.ramseytheory.com',
      descriptions: [
        'Progressed from Software Engineer to Lead Engineer, and currently serving as Head of Engineering.',
        'Trained and mentored other Lead Engineers to enhance team capabilities.',
        'Developed robust platforms using Django and Next.js, contributing to successful project outcomes.',
        'Established Rancher infrastructure to optimize CI/CD pipelines for efficient deployment.',
        'Organized and orchestrated multiple projects and teams, ensuring alignment and collaboration.',
        'Set up engineering hierarchy and standards to promote best practices and operational excellence.',
        'Drove startup project profitability through strategic planning and execution.',
        'Established and organized IT infrastructure by integrating Azure and Intune with Apple Business Manager (ABM) and Mobile Device Management (MDM) for streamlined device deployment and enhanced security.'
      ],
      from: DateTime.fromISO('2022-08-01'),
      to: null,
    },
    {
      title: 'Software Developer',
      company: 'Best Athletes',
      companyNameCss: 'hover:text-bestathletes',
      companyLink: 'https://bestathletes.co',
      descriptions: [
        'Successfully finalized the IRAP project by deploying MLFlow and machine learning models to Kubernetes, securing funding in the process.',
        'Transitioned BitBucket Pipelines to CircleCI, achieving a 60% reduction in build times.',
        'Developed a search engine using ElasticSearch to efficiently query information on universities, colleges, and sports.',
        'Integrated the Stripe API into a customer-facing application built with Vue (TypeScript) and Express to streamline customer payment processing.',
        'Lowered data costs by web scraping thousands of contact details using Python and Curl.',
      ],
      from: DateTime.fromISO('2021-01-01'),
      to: DateTime.fromISO('2022-07-31'),
    },
    {
      title: 'IT Lead/ Full Stack Developer',
      company: 'RK Publishing',
      companyNameCss: 'hover:text-rkpublishing',
      companyLink: 'https://rkpublishing.com/',
      descriptions: [
        'Streamlined the customer management dashboard, reducing onboarding time by 75%.',
        'Utilized Selenium to develop an automation script that reduced order processing time by 80%.',
        'Employed Test Driven Development to create a license management backend using Django REST Framework and Django-Guardian.',
        'Developed a messaging system with sound recording and attachment features to enhance user communication, utilizing PHP (Composer), AWS S3, MediaStream Recording API, and Polyfills for older iPads.',
        'Decreased support requests by establishing a Mail Server with OpenDKIM, Dovecot, and Postfix, and integrating automated password resets in PHP.',
        'Conducted interviews for software developers and support staff.',
        'Deployed LAMP stack development environments to support testing and development efforts.',
        'Recovered, extracted, organized, and archived essential project files from various sources.',
        'Set up automated backups for network share files to ensure data integrity.',
      ],
      from: DateTime.fromISO('2019-07-01'),
      to: DateTime.fromISO('2020-12-31'),
    },
    {
      title: 'Software Developer Team Lead',
      company: 'Ministry of Attorney General (Ontario)',
      companyNameCss: 'hover:text-attorneygeneral',
      companyLink: 'https://www.ontario.ca/page/ministry-attorney-general',
      descriptions: [
        'Led a team in developing a React Native prototype application for querying past court proceedings and providing notifications for upcoming proceedings, supported by a Django backend.',
        'Directed the creation of an automated web scraper and integrated Graphene to enable data queries through GraphQL.',
      ],
      from: DateTime.fromISO('2019-07-01'),
      to: DateTime.fromISO('2019-12-31'),
    },
  ];
  return (
    <div className={`text-slate-900`}>
      <h2 className="text-2xl pl-2">Work Experience</h2>

      <hr className="mt-1 mb-3"></hr>

      <div className="px-2 grid gap-3">
        {experienceItems.map((item, key) => (
          <div key={key}>
            <ExperienceItem {...item} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default ExperienceSection;
